# Research

A repository containing personal research-related data and code.
Code provided under the GNU GPLv3+ License.
Data provided under the GNU FDL License.